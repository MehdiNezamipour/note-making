package com.nezamipour.mehdi.notemaking.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Folder(
    val id: Long,
    var title: String,
) : ParentModel, Parcelable