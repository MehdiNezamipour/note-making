package com.nezamipour.mehdi.notemaking.ui.writenote

import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.text.toHtml
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.nezamipour.mehdi.notemaking.R
import com.nezamipour.mehdi.notemaking.databinding.FragmentWriteNoteBinding
import com.nezamipour.mehdi.notemaking.utils.ui_utils.DateUtils
import com.nezamipour.mehdi.notemaking.utils.ui_utils.UiUtils
import com.nezamipour.mehdi.notemaking.utils.ui_utils.showKeyboard
import dagger.hilt.android.AndroidEntryPoint
import java.util.*


@AndroidEntryPoint
class WriteNoteFragment : Fragment() {

    private val viewModel: WriteNoteViewModel by viewModels()
    private lateinit var binding: FragmentWriteNoteBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.note = WriteNoteFragmentArgs.fromBundle(requireArguments()).note
        viewModel.folderId = WriteNoteFragmentArgs.fromBundle(requireArguments()).folderId

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWriteNoteBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        setListeners()
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            insertNote()
        }
    }


    private fun initUi() {
        binding.editTextNoteExplain.customSelectionActionModeCallback =
            StyleCallback(binding.editTextNoteExplain)
        if (viewModel.note != null) {
            binding.editTextNoteTitle.setText(viewModel.note!!.title)
            binding.editTextNoteExplain.text =
                Html.fromHtml(
                    viewModel.note!!.explain,
                    Html.FROM_HTML_MODE_LEGACY
                ) as Editable?

            binding.textViewWriteNoteDate.text =
                DateUtils.gregorianToJalali(viewModel.note!!.date)
        } else {
            binding.editTextNoteTitle.requestFocus()
            requireView().showKeyboard()
            binding.textViewWriteNoteDate.text =
                DateUtils.gregorianToJalali(Date())
        }
    }


    private fun setListeners() {

        binding.imageViewBack.setOnClickListener {
            insertNote()
        }

        binding.imageViewWriteNoteMore.setOnClickListener {
            UiUtils.showPopupMenu(
                binding.root.context,
                it,
                R.menu.more_option_write_note
            ) { menuItem ->
                if (menuItem.itemId == R.id.delete_note) {
                    findNavController().previousBackStackEntry?.savedStateHandle?.set("hide", 0)
                    if (viewModel.note != null)
                        viewModel.note?.let { it1 ->
                            viewModel.deleteNote(it1)
                        }
                    Navigation.findNavController(it).popBackStack()

                }
                false
            }
        }
    }

    private fun insertNote() {
        // handle hide keyboard when back to home fragment.
        findNavController().previousBackStackEntry?.savedStateHandle?.set("hide", 0)
        // for add note with no  folder pass 0 for folderOfNoteId
        // because this note doesn't belong to folder
        if (viewModel.note == null && viewModel.folderId == 0L && binding.editTextNoteTitle.text.toString()
                .isNotEmpty()
        ) {
            binding.editTextNoteExplain.text?.let {
                viewModel.insertNoteWithoutFolder(
                    binding.editTextNoteTitle.text.toString(),
                    it.toHtml()
                )
            }
            findNavController().popBackStack()
        } else if (viewModel.note == null && viewModel.folderId != 0L && binding.editTextNoteTitle.text.toString()
                .isNotEmpty()
        ) {
            binding.editTextNoteExplain.text?.toHtml()?.let {
                viewModel.insertNoteWithFolder(
                    viewModel.folderId,
                    binding.editTextNoteTitle.text.toString(),
                    it
                )
            }
            findNavController().popBackStack()
        } else if (viewModel.note != null && binding.editTextNoteTitle.text.toString()
                .isNotEmpty()
        ) {
            binding.editTextNoteExplain.text?.toHtml()?.let {
                viewModel.updateNote(
                    binding.editTextNoteTitle.text.toString(),
                    it
                )
            }
            findNavController().popBackStack()
        } else
            findNavController().popBackStack()
    }


    companion object {
        @JvmStatic
        fun newInstance() =
            WriteNoteFragment().apply {
            }
    }
}