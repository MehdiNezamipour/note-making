package com.nezamipour.mehdi.notemaking.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nezamipour.mehdi.notemaking.data.entity.FolderEntity
import com.nezamipour.mehdi.notemaking.data.entity.FolderWithNotes
import com.nezamipour.mehdi.notemaking.model.Folder
import com.nezamipour.mehdi.notemaking.model.Note
import com.nezamipour.mehdi.notemaking.repository.NoteRepository
import com.nezamipour.mehdi.notemaking.utils.database_utils.DatabaseFolderMapper
import com.nezamipour.mehdi.notemaking.utils.database_utils.DatabaseNoteMapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val noteRepository: NoteRepository,
    private val databaseNoteMapper: DatabaseNoteMapper,
    private val databaseFolderMapper: DatabaseFolderMapper
) : ViewModel() {


    private val foldersWithNotes: MutableLiveData<List<FolderWithNotes>> = MutableLiveData()
    private val notes: MutableLiveData<List<Note>> = MutableLiveData()
    val folderWithNote: MutableLiveData<FolderWithNotes> = MutableLiveData()
    val note : MutableLiveData<Note> = MutableLiveData()


    /**
     * return MediatorLiveData that is merge of our two LiveData (notes, foldersWithNotes)
     */
    fun getAllData(): LiveDataMerger<Note, FolderWithNotes> {
        return LiveDataMerger(notes, foldersWithNotes)
    }


    fun getAllNotes() {
        viewModelScope.launch {
            noteRepository.getAllNotesWithoutFolder()
                .map {
                    databaseNoteMapper.mapFromEntityList(it)
                }
                .collect {
                    notes.postValue(it)
                }
        }
    }

    fun getFoldersWithNotes() {
        viewModelScope.launch {
            noteRepository.getFoldersWithNotes()
                .collect {
                    foldersWithNotes.postValue(it)
                }
        }
    }

    fun deleteNote() {
        viewModelScope.launch {
            note.value?.let { noteRepository.deleteNote(it) }
        }
    }


    fun mapFolderFromEntity(folderEntity: FolderEntity): Folder {
        return databaseFolderMapper.mapFromEntity(folderEntity)
    }

    private fun deleteNotesOfFolder() {
        viewModelScope.launch {
            folderWithNote.value?.let { noteRepository.deleteNotes(it.notes) }
        }
    }


    private fun deleteFolder() {
        viewModelScope.launch {
            folderWithNote.value?.let { noteRepository.deleteFolder(it.folderEntity) }
        }
    }

    fun deleteFolderWithNotes() {
        deleteFolder()
        deleteNotesOfFolder()
    }

    fun updateFolder(newTitle: String) {
        viewModelScope.launch {
            val newFolder = folderWithNote.value?.folderEntity?.let {
                Folder(
                    it.folderId,
                    newTitle
                )
            }
            newFolder?.let { noteRepository.updateFolder(it) }
        }
    }

}