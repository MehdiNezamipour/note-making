package com.nezamipour.mehdi.notemaking.utils.ui_utils

import java.util.*
import kotlin.collections.HashMap


class DateUtils {

    companion object {

        private val monthHashMap: HashMap<Int, String> = hashMapOf()

        init {
            monthHashMap[1] = "فروردین"
            monthHashMap[2] = "اردیبهشت"
            monthHashMap[3] = "خرداد"
            monthHashMap[4] = "تیر"
            monthHashMap[5] = "مرداد"
            monthHashMap[6] = "شهریور"
            monthHashMap[7] = "مهر"
            monthHashMap[8] = "آبان"
            monthHashMap[9] = "آذر"
            monthHashMap[10] = "دی"
            monthHashMap[11] = "بهمن"
            monthHashMap[12] = "اسفند"

        }

        private fun extractNumbersFromDate(date: Date): IntArray {
            val calendar = Calendar.getInstance()
            calendar.time = date
            return intArrayOf(
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            )
        }


        fun gregorianToJalali(date: Date): String {
            var days: Int
            val jm: Int
            val jd: Int
            val dateNumbers = extractNumbersFromDate(date)
            val gy = dateNumbers[0]
            val gm = dateNumbers[1]
            val gd = dateNumbers[2]

            run {
                val gy2 = if (gm > 2) gy + 1 else gy
                val g_d_m =
                    intArrayOf(0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334)
                days =
                    355666 + 365 * gy + ((gy2 + 3) / 4) - ((gy2 + 99) / 100) + ((gy2 + 399) / 400) + gd + g_d_m[gm - 1]
            }
            var jy = -1595 + 33 * (days / 12053)
            days %= 12053
            jy += 4 * (days / 1461)
            days %= 1461
            if (days > 365) {
                jy += ((days - 1) / 365)
                days = (days - 1) % 365
            }
            if (days < 186) {
                jm = 1 + (days / 31)
                jd = 1 + days % 31
            } else {
                jm = 7 + ((days - 186) / 30)
                jd = 1 + (days - 186) % 30
            }
            val jalali: String
            val month = when (jm) {
                1 -> monthHashMap[1]
                2 -> monthHashMap[2]
                3 -> monthHashMap[3]
                4 -> monthHashMap[4]
                5 -> monthHashMap[5]
                6 -> monthHashMap[6]
                7 -> monthHashMap[7]
                8 -> monthHashMap[8]
                9 -> monthHashMap[9]
                10 -> monthHashMap[10]
                11 -> monthHashMap[11]
                else -> monthHashMap[12]
            }
            jalali = "$jd\t$month\t\t${jy}"
            return jalali.formatWithLocal()
        }


        fun jalaliToGregorian(jy: Int, jm: Int, jd: Int): IntArray {
            var jy = jy
            jy += 1595
            var days =
                -355668 + 365 * jy + (jy / 33) * 8 + ((jy % 33 + 3) / 4) + jd + if (jm < 7) (jm - 1) * 31 else (jm - 7) * 30 + 186
            var gy = 400 * (days / 146097)
            days %= 146097
            if (days > 36524) {
                gy += 100 * (--days / 36524)
                days %= 36524
                if (days >= 365) days++
            }
            gy += 4 * (days / 1461)
            days %= 1461
            if (days > 365) {
                gy += ((days - 1) / 365)
                days = (days - 1) % 365
            }
            var gm: Int
            var gd = days + 1
            run {
                val sal_a = intArrayOf(
                    0,
                    31,
                    if (gy % 4 == 0 && gy % 100 != 0 || gy % 400 == 0) 29 else 28,
                    31,
                    30,
                    31,
                    30,
                    31,
                    31,
                    30,
                    31,
                    30,
                    31
                )
                gm = 0
                while (gm < 13 && gd > sal_a[gm]) {
                    gd -= sal_a[gm]
                    gm++
                }
            }
            return intArrayOf(gy, gm, gd)
        }
    }

}