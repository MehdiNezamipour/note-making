package com.nezamipour.mehdi.notemaking.ui.writenote

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.CharacterStyle
import android.text.style.StyleSpan
import android.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import com.nezamipour.mehdi.notemaking.R


class StyleCallback(private val editText: EditText) :
    ActionMode.Callback {


    override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        val inflater = mode?.menuInflater

        menu?.removeItem(android.R.id.cut)
        menu?.removeItem(android.R.id.paste)
        menu?.removeItem(android.R.id.shareText)
        menu?.removeItem(android.R.id.shareText)


        val copy = menu?.findItem(android.R.id.copy)
        copy?.setTitle(R.string.copy_text)
        copy?.setIcon(R.drawable.ic_copy)

        val paste = menu?.findItem(android.R.id.paste)
        paste?.setTitle(R.string.paste_text)
        paste?.setIcon(R.drawable.ic_paste)

        val selectAll = menu?.findItem(android.R.id.selectAll)
        selectAll?.setTitle(R.string.select_all)

        inflater?.inflate(R.menu.text_option_menu, menu)
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        return false
    }

    override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
        val cs: CharacterStyle
        val start: Int = editText.selectionStart
        val end: Int = editText.selectionEnd
        val totalText = SpannableStringBuilder(editText.text)
        val selectedText = SpannableStringBuilder(editText.text, start, end)
        val selectedTextSpans = selectedText.getSpans(0, selectedText.length, StyleSpan::class.java)

        when (item?.itemId) {
            R.id.bold_text -> {
                return if (selectedTextSpans.isEmpty()
                    || !checkBold(selectedTextSpans)
                ) {
                    cs = StyleSpan(Typeface.BOLD)
                    totalText.setSpan(cs, start, end, 1)
                    editText.text = totalText
                    true
                } else
                    true
            }
            R.id.italic_text -> {
                return if (selectedTextSpans.isEmpty()
                    || !checkItalic(selectedTextSpans)
                ) {
                    cs = StyleSpan(Typeface.ITALIC)
                    totalText.setSpan(cs, start, end, 1)
                    editText.text = totalText
                    true
                } else
                    true
            }
            R.id.normal_text -> {
                removeAllSpans(totalText, start, end)
                editText.text = totalText
                return true
            }

        }
        return false
    }

    override fun onDestroyActionMode(mode: ActionMode?) {

    }

    private fun checkBold(spans: Array<StyleSpan>): Boolean {
        for (span in spans) {
            if (span.style == Typeface.BOLD) {
                return true
            }
        }
        return false
    }

    private fun checkItalic(spans: Array<StyleSpan>): Boolean {
        for (span in spans) {
            if (span.style == Typeface.ITALIC) {
                return true
            }
        }
        return false
    }


    private fun removeAllSpans(spannable: Spannable, startSelection: Int, endSelection: Int) {
        val spansToRemove = spannable.getSpans(
            startSelection, endSelection,
            Any::class.java
        )
        for (span in spansToRemove) {
            if (span is CharacterStyle) spannable.removeSpan(span)
        }
    }

}