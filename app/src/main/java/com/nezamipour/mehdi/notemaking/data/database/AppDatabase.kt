package com.nezamipour.mehdi.notemaking.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.nezamipour.mehdi.notemaking.data.dao.NoteDao
import com.nezamipour.mehdi.notemaking.data.entity.FolderEntity
import com.nezamipour.mehdi.notemaking.data.entity.NoteEntity

@Database(entities = [NoteEntity::class, FolderEntity::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun noteDao(): NoteDao

}