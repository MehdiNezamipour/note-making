package com.nezamipour.mehdi.notemaking.data.entity

import androidx.room.Embedded
import androidx.room.Relation
import com.nezamipour.mehdi.notemaking.model.ParentModel

data class FolderWithNotes(
    @Embedded val folderEntity: FolderEntity,
    @Relation(
        parentColumn = "folderId",
        entityColumn = "folderOfNoteId"
    )
    val notes: List<NoteEntity>
) : ParentModel
