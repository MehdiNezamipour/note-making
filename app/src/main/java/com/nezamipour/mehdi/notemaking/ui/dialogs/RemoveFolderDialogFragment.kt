package com.nezamipour.mehdi.notemaking.ui.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.nezamipour.mehdi.notemaking.databinding.FragmentRemoveFolderDialogBinding
import dagger.hilt.android.AndroidEntryPoint

class RemoveFolderDialogFragment : DialogFragment() {

    private lateinit var binding: FragmentRemoveFolderDialogBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRemoveFolderDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setListeners()
    }

    private fun setListeners() {
        binding.buttonDeleteFolder.setOnClickListener {
            findNavController().previousBackStackEntry?.savedStateHandle?.set(
                "delete",
                true
            )
            dismiss()
        }
        binding.buttonCancel.setOnClickListener {
            dismiss()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            RemoveFolderDialogFragment().apply {
            }
    }
}