package com.nezamipour.mehdi.notemaking.adapter

import androidx.recyclerview.widget.RecyclerView
import com.nezamipour.mehdi.notemaking.R
import com.nezamipour.mehdi.notemaking.databinding.NoteItemLayoutBinding
import com.nezamipour.mehdi.notemaking.model.Note
import com.nezamipour.mehdi.notemaking.utils.ui_utils.DateUtils
import com.nezamipour.mehdi.notemaking.utils.ui_utils.UiUtils


class NoteViewHolder(
    private val binding: NoteItemLayoutBinding,
    private val onNoteClickListener: OnNoteClickListener,
    private val onDeleteNoteClickListener: OnDeleteNoteClickListener
) : RecyclerView.ViewHolder(binding.root) {


    var note: Note? = null

    fun bind(note: Note) {
        this.note = note
        binding.textViewNoteTitle.text = note.title
        binding.textViewNoteTime.text =
            DateUtils.gregorianToJalali(note.date)
        binding.root.setOnClickListener {
            onNoteClickListener.onNoteClicked(note)
        }
        binding.imageViewNoteMoreOption.setOnClickListener {
            UiUtils.showPopupMenu(
                binding.root.context,
                it,
                R.menu.more_option_write_note
            ) { menuItem ->
                if (menuItem.itemId == R.id.delete_note) {
                    onDeleteNoteClickListener.onClickedDelete(note)
                }
                false
            }

        }
    }


}

interface OnNoteClickListener {
    fun onNoteClicked(note: Note)
}

interface OnDeleteNoteClickListener {
    fun onClickedDelete(note: Note)
}
