package com.nezamipour.mehdi.notemaking.ui.dialogs

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nezamipour.mehdi.notemaking.data.entity.FolderEntity
import com.nezamipour.mehdi.notemaking.data.entity.NoteEntity
import com.nezamipour.mehdi.notemaking.model.Folder
import com.nezamipour.mehdi.notemaking.repository.NoteRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DialogsViewModel @Inject constructor(private val noteRepository: NoteRepository) :
    ViewModel() {
    var folderId: Long = 0L
    lateinit var folder: Folder


    fun insertFolder(folderTitle: String) {
        viewModelScope.launch {
            val folder = Folder(
                0,
                folderTitle
            )
            noteRepository.insertFolder(folder)
        }

    }

}