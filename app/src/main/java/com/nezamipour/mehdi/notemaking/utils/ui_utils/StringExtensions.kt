package com.nezamipour.mehdi.notemaking.utils.ui_utils

import java.text.NumberFormat
import java.util.*

fun String.formatWithLocal(): String =
    this.map { if (it.isDigit()) it.formatWithLocal() else it }.joinToString("")


fun Char.formatWithLocal(): Char =
    if (isDigit()) NumberFormat.getInstance(Locale(Locale("fa").language, "IR"))
        .format(this.toString().toByte()).first() else this

inline fun <reified T : Number> T.formatWithLocal(): String {
    return String.format(Locale(Locale("fa").language, "IR"), "%d", this)


}