package com.nezamipour.mehdi.notemaking.ui.foldernotes

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.nezamipour.mehdi.notemaking.model.Folder
import com.nezamipour.mehdi.notemaking.model.Note
import com.nezamipour.mehdi.notemaking.repository.NoteRepository
import com.nezamipour.mehdi.notemaking.utils.database_utils.DatabaseNoteMapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FolderNotesViewModel @Inject constructor(
    private val noteRepository: NoteRepository,
    private val databaseNoteMapper: DatabaseNoteMapper
) : ViewModel() {

    val folder: MutableLiveData<Folder> = MutableLiveData()
    val notes: MutableLiveData<List<Note>> = MutableLiveData()
    val note: MutableLiveData<Note> = MutableLiveData()
    val paging: MutableLiveData<PagingData<Note>> = MutableLiveData()

    fun getNotesOfFolderPaged(): Flow<PagingData<Note>> {
        return Pager(
            PagingConfig(pageSize = 20)
        ) {
            noteRepository.getNotesOfFolderPaged(folder.value!!.id)
        }.flow.map { pagingData ->
            pagingData.map {
                databaseNoteMapper.mapFromEntity(it)
            }
        }
            .cachedIn(viewModelScope)
    }

    fun getNotesOfFolder(folderId: Long) {
        viewModelScope.launch {
            noteRepository.getNotesOfFolder(folderId).collect {
                notes.postValue(it)
            }
        }
    }

    fun deleteNote() {
        viewModelScope.launch {
            note.value?.let { noteRepository.deleteNote(it) }
        }
    }

    fun updateFolder(newTitle: String) {
        viewModelScope.launch {
            val newFolder = folder.value?.let {
                Folder(
                    id = it.id,
                    newTitle
                )
            }
            newFolder?.let { noteRepository.updateFolder(it) }
        }
    }

    fun deleteNotesOfFolder(folderId: Long) {
        viewModelScope.launch {
            noteRepository.deleteNotesOfFolder(folderId)
        }
    }


    fun deleteFolderById(folderId: Long) {
        viewModelScope.launch {
            noteRepository.deleteFolderById(folderId)
        }
    }


}
