package com.nezamipour.mehdi.notemaking.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class Note(
    val id: Long,
    val folderOfNoteId: Long,
    val title: String,
    val explain: String,
    val date: Date
) : ParentModel, Parcelable