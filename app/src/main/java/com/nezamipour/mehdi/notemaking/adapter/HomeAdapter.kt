package com.nezamipour.mehdi.notemaking.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nezamipour.mehdi.notemaking.data.entity.FolderWithNotes
import com.nezamipour.mehdi.notemaking.databinding.FolderItemLayoutBinding
import com.nezamipour.mehdi.notemaking.databinding.NoteItemLayoutBinding
import com.nezamipour.mehdi.notemaking.model.Note
import com.nezamipour.mehdi.notemaking.model.ParentModel
import com.nezamipour.mehdi.notemaking.ui.home.HomeViewModel
import javax.inject.Inject

class HomeAdapter @Inject constructor() :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    // ViewHolders Listeners
    private lateinit var onNoteClickListener: OnNoteClickListener
    private lateinit var onDeleteNoteClickListener: OnDeleteNoteClickListener
    private lateinit var onFolderClickListener: OnFolderClickListener
    private lateinit var onFolderDeleteClickListener: OnFolderDeleteClickListener
    private lateinit var onFolderRenameClickListener: OnFolderRenameClickListener


    private val items = ArrayList<ParentModel>()

    @JvmName("setOnNoteClickListener1")
    fun setOnNoteClickListener(onNoteClickListener: OnNoteClickListener) {
        this.onNoteClickListener = onNoteClickListener
    }

    @JvmName("setOnDeleteNoteClickListener1")
    fun setOnDeleteNoteClickListener(onDeleteNoteClickListener: OnDeleteNoteClickListener) {
        this.onDeleteNoteClickListener = onDeleteNoteClickListener
    }

    @JvmName("setOnFolderClickListener1")
    fun setOnFolderClickListener(onFolderClickListener: OnFolderClickListener) {
        this.onFolderClickListener = onFolderClickListener
    }

    @JvmName("setOnFolderRenameClickListener1")
    fun setOnFolderRenameClickListener(onFolderRenameClickListener: OnFolderRenameClickListener) {
        this.onFolderRenameClickListener = onFolderRenameClickListener
    }

    @JvmName("setOnFolderDeleteClickListener1")
    fun setOnFolderDeleteClickListener(onFolderDeleteClickListener: OnFolderDeleteClickListener) {
        this.onFolderDeleteClickListener = onFolderDeleteClickListener
    }


    fun setItems(list: List<ParentModel>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }


    companion object {
        const val VIEW_TYPE_NOTE = 1
        const val VIEW_TYPE_FOLDER = 2
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            VIEW_TYPE_NOTE -> return NoteViewHolder(
                NoteItemLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ), onNoteClickListener, onDeleteNoteClickListener
            )
            VIEW_TYPE_FOLDER -> return FolderViewHolder(
                FolderItemLayoutBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ), onFolderClickListener, onFolderDeleteClickListener, onFolderRenameClickListener
            )
            else -> throw Exception("class not defined")
        }

    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is Note -> VIEW_TYPE_NOTE
            is FolderWithNotes -> VIEW_TYPE_FOLDER
            else -> 0
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            VIEW_TYPE_NOTE -> {
                val noteViewHolder = holder as NoteViewHolder
                noteViewHolder.bind(items[position] as Note)
            }
            VIEW_TYPE_FOLDER -> {
                val folderViewHolder = holder as FolderViewHolder
                folderViewHolder.bind(items[position] as FolderWithNotes)
            }
            else -> throw Exception("class not defined")
        }

    }

    override fun getItemCount(): Int {
        return items.size
    }

}