package com.nezamipour.mehdi.notemaking.adapter

import androidx.recyclerview.widget.RecyclerView
import com.nezamipour.mehdi.notemaking.R
import com.nezamipour.mehdi.notemaking.data.entity.FolderWithNotes
import com.nezamipour.mehdi.notemaking.databinding.FolderItemLayoutBinding
import com.nezamipour.mehdi.notemaking.utils.ui_utils.UiUtils

class FolderViewHolder(
    private val binding: FolderItemLayoutBinding,
    private val onFolderClickListener: OnFolderClickListener,
    private val onFolderDeleteClickListener: OnFolderDeleteClickListener,
    private val onFolderRenameClickListener: OnFolderRenameClickListener
) :
    RecyclerView.ViewHolder(binding.root) {

    var folderWithNotes: FolderWithNotes? = null

    fun bind(folderWithNotes: FolderWithNotes) {
        this.folderWithNotes = folderWithNotes
        binding.textViewFolderTitle.text = folderWithNotes.folderEntity.title
        if (folderWithNotes.notes.isEmpty())
            binding.textViewNumberOfNotes.text = binding.root.resources.getString(
                R.string.emptyFolder
            )
        else
            binding.textViewNumberOfNotes.text = binding.root.resources.getString(
                R.string.numberOfNotesInFolder, folderWithNotes.notes.size
            )
        setListeners()
    }

    private fun setListeners() {
        binding.imageViewFolderMoreOption.setOnClickListener {
            UiUtils.showPopupMenu(
                binding.root.context,
                it,
                R.menu.more_option_folder_menu
            ) { menuItem ->
                when (menuItem.itemId) {
                    R.id.rename_folder -> {
                        folderWithNotes?.let { it1 ->
                            onFolderRenameClickListener.onFolderRenameClicked(
                                it1
                            )
                        }
                    }
                    R.id.delete_folder -> {
                        folderWithNotes?.let { it1 ->
                            onFolderDeleteClickListener.onFolderDeleteClicked(
                                it1
                            )
                        }
                    }
                }
                false
            }
        }
        binding.root.setOnClickListener {
            folderWithNotes?.let { it1 -> onFolderClickListener.onFolderClicked(it1) }
        }
    }

}

interface OnFolderRenameClickListener {
    fun onFolderRenameClicked(folderWithNotes: FolderWithNotes)
}

interface OnFolderDeleteClickListener {
    fun onFolderDeleteClicked(folderWithNotes: FolderWithNotes)
}

interface OnFolderClickListener {
    fun onFolderClicked(folderWithNotes: FolderWithNotes)
}
