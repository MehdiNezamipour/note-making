package com.nezamipour.mehdi.notemaking.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "folder")
data class FolderEntity(
    @PrimaryKey(autoGenerate = true)
    val folderId: Long,
    val title: String,
)