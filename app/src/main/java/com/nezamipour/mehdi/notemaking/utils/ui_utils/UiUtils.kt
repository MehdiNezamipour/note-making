package com.nezamipour.mehdi.notemaking.utils.ui_utils

import android.app.Activity
import android.content.Context
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.PopupMenu
import java.lang.reflect.Method

class UiUtils {

    companion object {
        // method to show popup menu
        fun showPopupMenu(
            context: Context,
            view: View,
            menuResourceId: Int,
            menuItemClickListener: PopupMenu.OnMenuItemClickListener
        ) {
            val popup = PopupMenu(context, view)
            popup.apply {
                // inflate the popup menu
                menuInflater.inflate(menuResourceId, menu)
                // popup menu item click listener
                setOnMenuItemClickListener(menuItemClickListener)
            }
            // show icons on popup menu
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                popup.setForceShowIcon(true)
            } else {
                try {
                    val fields = popup.javaClass.declaredFields
                    for (field in fields) {
                        if ("mPopup" == field.name) {
                            field.isAccessible = true
                            val menuPopupHelper = field[popup]
                            val classPopupHelper =
                                Class.forName(menuPopupHelper.javaClass.name)
                            val setForceIcons: Method = classPopupHelper.getMethod(
                                "setForceShowIcon",
                                Boolean::class.javaPrimitiveType
                            )
                            setForceIcons.invoke(menuPopupHelper, true)
                            break
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            popup.show()
        }



    }

}