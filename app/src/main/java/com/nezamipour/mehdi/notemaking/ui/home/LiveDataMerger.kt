package com.nezamipour.mehdi.notemaking.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

class LiveDataMerger<Type1, Type2>(
    type1: LiveData<List<Type1>>,
    type2: LiveData<List<Type2>>
) : MediatorLiveData<Pair<List<Type1>, List<Type2>>>() {

    private var listType1: List<Type1> = emptyList()
    private var listType2: List<Type2> = emptyList()

    init {
        value = Pair(listType1, listType2)
        addSource(type1) {
            if (it != null) listType1 = it
            value = Pair(listType1, listType2)
        }
        addSource(type2) {
            if (it != null) listType2 = it
            value = Pair(listType1, listType2)
        }
    }
}