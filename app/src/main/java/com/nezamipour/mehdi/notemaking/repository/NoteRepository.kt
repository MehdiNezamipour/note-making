package com.nezamipour.mehdi.notemaking.repository

import androidx.paging.*
import com.nezamipour.mehdi.notemaking.data.dao.NoteDao
import com.nezamipour.mehdi.notemaking.data.entity.FolderEntity
import com.nezamipour.mehdi.notemaking.data.entity.FolderWithNotes
import com.nezamipour.mehdi.notemaking.data.entity.NoteEntity
import com.nezamipour.mehdi.notemaking.model.Folder
import com.nezamipour.mehdi.notemaking.model.Note
import com.nezamipour.mehdi.notemaking.utils.database_utils.DatabaseFolderMapper
import com.nezamipour.mehdi.notemaking.utils.database_utils.DatabaseNoteMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class NoteRepository @Inject constructor(
    private val noteDao: NoteDao,
    private val databaseNoteMapper: DatabaseNoteMapper,
    private val databaseFolderMapper: DatabaseFolderMapper
) {

    // initial test
    suspend fun insertNote(note: Note): Long {
        return noteDao.insertNote(databaseNoteMapper.mapToEntity(note))
    }

    suspend fun insertFolder(folder: Folder): Long {
        return noteDao.insertFolder(databaseFolderMapper.mapToEntity(folder))
    }

    suspend fun deleteNote(note: Note) {
        noteDao.deleteNote(databaseNoteMapper.mapToEntity(note))
    }

    suspend fun deleteNotesOfFolder(folderId: Long) {
        noteDao.deleteNotesOfFolder(folderId)
    }

    suspend fun deleteNotes(notes: List<NoteEntity>) {
        noteDao.deleteNotes(notes)
    }


    suspend fun deleteFolder(folder: FolderEntity) {
        noteDao.deleteFolder(folder)
    }

    suspend fun deleteFolderById(folderId: Long) {
        noteDao.deleteFolderById(folderId)
    }

    suspend fun updateNote(note: Note) {
        noteDao.updateNote(databaseNoteMapper.mapToEntity(note))
    }

    suspend fun updateFolder(folder: Folder) {
        noteDao.updateFolder(databaseFolderMapper.mapToEntity(folder))
    }


    // Get Queries


    fun getNote(noteId: Long): Flow<NoteEntity> {
        return noteDao.getNote(noteId)
    }


    fun getFolder(folderId: Long): Flow<FolderEntity> {
        return noteDao.getFolder(folderId)
    }

    fun getAllNotesWithoutFolder(): Flow<List<NoteEntity>> {
        return noteDao.getAllNotesWithoutFolder()
    }


    fun getAllNotes(): Flow<List<NoteEntity>> {
        return noteDao.getAllNotes()
    }

    fun getFoldersWithNotes(): Flow<List<FolderWithNotes>> {
        return noteDao.getFoldersWithNotes()
    }

    // use paging for notes of folder
    fun getNotesOfFolderPaged(folderId: Long): PagingSource<Int, NoteEntity> {
        return noteDao.getNotesOfFolderPaged(folderId)
    }


    fun getNotesOfFolder(folderId: Long): Flow<List<Note>> {
        return noteDao.getNotesOfFolder(folderId).map {
            databaseNoteMapper.mapFromEntityList(it)
        }
    }

    fun getAllFolders(): Flow<List<FolderEntity>> {
        return noteDao.getAllFolders()
    }


}