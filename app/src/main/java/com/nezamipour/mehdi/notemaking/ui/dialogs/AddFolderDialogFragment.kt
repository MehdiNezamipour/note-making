package com.nezamipour.mehdi.notemaking.ui.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.nezamipour.mehdi.notemaking.databinding.FragmentAddFolderDialogBinding
import com.nezamipour.mehdi.notemaking.ui.home.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddFolderDialogFragment : DialogFragment() {

    private lateinit var binding: FragmentAddFolderDialogBinding
    private val viewModel: DialogsViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddFolderDialogBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setListeners()
    }

    private fun setListeners() {
        binding.buttonCreateFolder.setOnClickListener {
            viewModel.insertFolder(
                binding.editTextFolderTitle.text.toString()
            )
            dismiss()
        }
        binding.buttonCancel.setOnClickListener {
            dismiss()
        }
    }


    companion object {
        @JvmStatic
        fun newInstance() =
            AddFolderDialogFragment().apply {
            }
    }
}