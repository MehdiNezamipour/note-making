package com.nezamipour.mehdi.notemaking.utils.database_utils

import com.nezamipour.mehdi.notemaking.data.entity.FolderEntity
import com.nezamipour.mehdi.notemaking.model.Folder
import javax.inject.Inject

class DatabaseFolderMapper @Inject constructor() : EntityMapper<FolderEntity, Folder> {

    override fun mapFromEntity(entity: FolderEntity): Folder {
        return Folder(
            entity.folderId,
            entity.title,
        )
    }

    override fun mapToEntity(domainModel: Folder): FolderEntity {
        return FolderEntity(
            domainModel.id,
            domainModel.title,
        )
    }

    fun mapFromEntityList(entities: List<FolderEntity>): List<Folder> {
        return entities.map { mapFromEntity(it) }
    }

    fun mapToEntityList(folders: List<Folder>): List<FolderEntity> {
        return folders.map { mapToEntity(it) }
    }

}