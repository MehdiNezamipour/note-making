package com.nezamipour.mehdi.notemaking

import android.app.Application
import android.util.Log
import com.nezamipour.mehdi.notemaking.utils.database_utils.DateConverter
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
// merge develop with master v1.2
// merge develop with master V1.3
// merge develop with master v1.4
// merge develop with master v1.5
// merge develop with master v1.6
// merge develop with master v1.7

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        // test
        /*  val jalali = DateConverter.gregorianToJalali(2021, 6, 3)
          Log.d("MyApplication", "onCreate: ${jalali} ")*/
    }

}