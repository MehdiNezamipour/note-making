package com.nezamipour.mehdi.notemaking.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.nezamipour.mehdi.notemaking.R
import com.nezamipour.mehdi.notemaking.adapter.*
import com.nezamipour.mehdi.notemaking.data.entity.FolderWithNotes
import com.nezamipour.mehdi.notemaking.databinding.FragmentHomeBinding
import com.nezamipour.mehdi.notemaking.model.Note
import com.nezamipour.mehdi.notemaking.model.ParentModel
import com.nezamipour.mehdi.notemaking.utils.ui_utils.ViewAnimation
import com.nezamipour.mehdi.notemaking.utils.ui_utils.hideKeyboard
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class HomeFragment : Fragment() {


    private lateinit var binding: FragmentHomeBinding

    @Inject
    lateinit var adapter: HomeAdapter
    private val viewModel: HomeViewModel by viewModels()
    private var isRotate: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getAllNotes()
        viewModel.getFoldersWithNotes()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewAnimation.init(binding.fabAddNote)
        ViewAnimation.init(binding.fabAddFolder)

        initUi()
        setObservers()
        setListeners()

    }

    private fun initUi() {
        binding.homeRecyclerView.adapter = adapter
        adapter.setOnDeleteNoteClickListener(object : OnDeleteNoteClickListener {
            override fun onClickedDelete(note: Note) {
                viewModel.note.value = note
                Navigation.findNavController(binding.root).navigate(
                    HomeFragmentDirections.actionHomeFragmentToRemoveNoteDialogFragment()
                )
            }

        })
        adapter.setOnNoteClickListener(object : OnNoteClickListener {
            override fun onNoteClicked(note: Note) {
                Navigation.findNavController(binding.root)
                    .navigate(HomeFragmentDirections.actionHomeFragmentToWriteNoteFragment(note))
            }

        })
        adapter.setOnFolderClickListener(object : OnFolderClickListener {
            override fun onFolderClicked(folderWithNotes: FolderWithNotes) {
                Navigation.findNavController(binding.root).navigate(
                    HomeFragmentDirections.actionHomeFragmentToFolderNotesFragment(
                        viewModel.mapFolderFromEntity(
                            folderEntity = folderWithNotes.folderEntity
                        )
                    )
                )

            }

        })
        adapter.setOnFolderDeleteClickListener(object : OnFolderDeleteClickListener {
            override fun onFolderDeleteClicked(folderWithNotes: FolderWithNotes) {
                viewModel.folderWithNote.value = folderWithNotes
                Navigation.findNavController(binding.root)
                    .navigate(
                        HomeFragmentDirections.actionHomeFragmentToRemoveFolderDialogFragment()
                    )

            }


        })
        adapter.setOnFolderRenameClickListener(object : OnFolderRenameClickListener {
            override fun onFolderRenameClicked(folderWithNotes: FolderWithNotes) {
                viewModel.folderWithNote.value = folderWithNotes
                Navigation.findNavController(binding.root)
                    .navigate(
                        HomeFragmentDirections.actionHomeFragmentToChangeFolderTitleDialogFragment(
                            viewModel.mapFolderFromEntity(folderEntity = folderWithNotes.folderEntity)
                        )
                    )
            }

        })

    }

    private fun setObservers() {
        // mediator live data
        viewModel.getAllData().observe(viewLifecycleOwner, {
            val all = ArrayList<ParentModel>()
            all.addAll(it.second)
            all.addAll(it.first)
            adapter.setItems(all)
        })

        requireContext()

        // after back from dialog
        val navController = findNavController()
        val navBackStackEntry = navController.getBackStackEntry(R.id.homeFragment)
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("hide")
            ?.observe(
                viewLifecycleOwner
            ) {
                requireView().hideKeyboard()
            }

        // observer for Delete folder
        val deleteFolderObserver = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_RESUME
                && navBackStackEntry.savedStateHandle.contains("delete")
            ) {
                val result = navBackStackEntry.savedStateHandle.get<Boolean>("delete")
                if (result == true) {
                    viewModel.deleteFolderWithNotes()
                }
            }
        }
        navBackStackEntry.lifecycle.addObserver(deleteFolderObserver)
        viewLifecycleOwner.lifecycle.addObserver(LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_DESTROY) {
                navBackStackEntry.lifecycle.removeObserver(deleteFolderObserver)
            }
        })

        // observer for rename folder
        val renameFolderObserver = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_RESUME
                && navBackStackEntry.savedStateHandle.contains("rename")
            ) {
                val newTitle = navBackStackEntry.savedStateHandle.get<String>("rename")
                if (newTitle != null) {
                    if (newTitle.isNotEmpty()) {
                        viewModel.updateFolder(newTitle)
                    }
                }
            }
        }
        navBackStackEntry.lifecycle.addObserver(renameFolderObserver)
        viewLifecycleOwner.lifecycle.addObserver(LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_DESTROY) {
                navBackStackEntry.lifecycle.removeObserver(renameFolderObserver)
            }
        })

        val deleteNoteObserver = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_RESUME
                && navBackStackEntry.savedStateHandle.contains("deleteNote")
            ) {
                val result = navBackStackEntry.savedStateHandle.get<Boolean>("deleteNote")
                if (result == true) {
                    viewModel.deleteNote()
                }
            }
        }
        navBackStackEntry.lifecycle.addObserver(deleteNoteObserver)
        viewLifecycleOwner.lifecycle.addObserver(LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_DESTROY) {
                navBackStackEntry.lifecycle.removeObserver(deleteNoteObserver)
            }
        })
    }

    private fun setListeners() {
        binding.fabAdd.setOnClickListener {
            floatingButtonAnim(it)
        }
        binding.fabAddFolder.setOnClickListener {
            floatingButtonAnim(binding.fabAdd)
            Navigation.findNavController(it)
                .navigate(HomeFragmentDirections.actionHomeFragmentToAddFolderDialogFragment())


        }

        binding.fabAddNote.setOnClickListener {
            isRotate = false
            Navigation.findNavController(it)
                .navigate(
                    HomeFragmentDirections.actionHomeFragmentToWriteNoteFragment(null)
                        .setFolderId(0)
                )
        }

        binding.dimScreen.setOnClickListener {
            floatingButtonAnim(binding.fabAdd)
        }

    }

    private fun floatingButtonAnim(it: View) {
        isRotate = ViewAnimation.rotateFab(it, !isRotate)
        if (isRotate) {
            ViewAnimation.showIn(binding.fabAddFolder)
            ViewAnimation.showIn(binding.fabAddNote)
            dimScreen()
        } else {
            ViewAnimation.showOut(binding.fabAddFolder)
            ViewAnimation.showOut(binding.fabAddNote)
            lightScreen()
        }
    }

    private fun dimScreen() {
        ViewAnimation.dimScreenAnim(binding.dimScreen)
    }

    private fun lightScreen() {
        ViewAnimation.lightScreenAnim(binding.dimScreen)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            HomeFragment().apply {

            }
    }

}