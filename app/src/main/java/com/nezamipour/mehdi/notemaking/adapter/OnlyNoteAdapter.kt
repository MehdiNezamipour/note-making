package com.nezamipour.mehdi.notemaking.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nezamipour.mehdi.notemaking.databinding.NoteItemLayoutBinding
import com.nezamipour.mehdi.notemaking.model.Note
import com.nezamipour.mehdi.notemaking.model.ParentModel
import javax.inject.Inject

class OnlyNoteAdapter @Inject constructor() :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    // ViewHolders Listeners
    private lateinit var onNoteClickListener: OnNoteClickListener
    private lateinit var onDeleteNoteClickListener: OnDeleteNoteClickListener


    private val items = ArrayList<ParentModel>()

    @JvmName("setOnNoteClickListener2")
    fun setOnNoteClickListener(onNoteClickListener: OnNoteClickListener) {
        this.onNoteClickListener = onNoteClickListener
    }

    @JvmName("setOnDeleteNoteClickListener2")
    fun setOnDeleteNoteClickListener(onDeleteNoteClickListener: OnDeleteNoteClickListener) {
        this.onDeleteNoteClickListener = onDeleteNoteClickListener
    }


    fun setItems(list: List<ParentModel>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NoteViewHolder(
            NoteItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), onNoteClickListener, onDeleteNoteClickListener
        )
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val noteViewHolder = holder as NoteViewHolder
        noteViewHolder.bind(items[position] as Note)

    }

    override fun getItemCount(): Int {
        return items.size
    }

}