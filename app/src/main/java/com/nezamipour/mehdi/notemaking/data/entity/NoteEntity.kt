package com.nezamipour.mehdi.notemaking.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "note")
data class NoteEntity(
    @PrimaryKey(autoGenerate = true)
    val noteId: Long,
    val folderOfNoteId: Long,
    val title: String,
    val explain: String,
    val date: String
)
