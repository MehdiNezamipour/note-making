package com.nezamipour.mehdi.notemaking.utils.database_utils

import com.nezamipour.mehdi.notemaking.data.database.Converters
import com.nezamipour.mehdi.notemaking.data.entity.NoteEntity
import com.nezamipour.mehdi.notemaking.model.Note
import java.util.*
import javax.inject.Inject

class DatabaseNoteMapper @Inject constructor() : EntityMapper<NoteEntity, Note> {

    override fun mapFromEntity(entity: NoteEntity): Note {
        return Note(
            entity.noteId,
            entity.folderOfNoteId,
            entity.title,
            entity.explain,
            Converters.stringToDate(entity.date)
        )
    }

    override fun mapToEntity(domainModel: Note): NoteEntity {
        return NoteEntity(
            domainModel.id,
            domainModel.folderOfNoteId,
            domainModel.title,
            domainModel.explain,
            Converters.dateToString(domainModel.date)
        )
    }

    fun mapFromEntityList(entities: List<NoteEntity>): List<Note> {
        return entities.map { mapFromEntity(it) }
    }

    fun mapToEntityList(notes: List<Note>): List<NoteEntity> {
        return notes.map { mapToEntity(it) }
    }
}