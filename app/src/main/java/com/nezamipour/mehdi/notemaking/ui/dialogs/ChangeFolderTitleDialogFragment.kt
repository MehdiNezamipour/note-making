package com.nezamipour.mehdi.notemaking.ui.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.nezamipour.mehdi.notemaking.databinding.FragmentChangeFolderTitleDialogBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChangeFolderTitleDialogFragment : DialogFragment() {

    private lateinit var binding: FragmentChangeFolderTitleDialogBinding

    private val viewModel: DialogsViewModel by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.folder = ChangeFolderTitleDialogFragmentArgs.fromBundle(requireArguments()).folder

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChangeFolderTitleDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        setListeners()
    }

    private fun initUi() {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding.editTextFolderTitle.setText(viewModel.folder.title)
    }

    private fun setListeners() {
        binding.buttonSave.setOnClickListener {
            findNavController().previousBackStackEntry?.savedStateHandle?.set(
                "rename",
                binding.editTextFolderTitle.text.toString()
            )
            dismiss()
        }

        binding.buttonCancel.setOnClickListener {
            dismiss()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ChangeFolderTitleDialogFragment().apply {
            }
    }
}