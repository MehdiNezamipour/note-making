package com.nezamipour.mehdi.notemaking.ui.foldernotes.paging

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.nezamipour.mehdi.notemaking.adapter.NoteViewHolder
import com.nezamipour.mehdi.notemaking.adapter.OnDeleteNoteClickListener
import com.nezamipour.mehdi.notemaking.adapter.OnNoteClickListener
import com.nezamipour.mehdi.notemaking.databinding.NoteItemLayoutBinding
import com.nezamipour.mehdi.notemaking.model.Note
import javax.inject.Inject

class NotePagingAdapter @Inject constructor() :
    PagingDataAdapter<Note, NoteViewHolder>(DIFF_CALLBACK) {


    // ViewHolders Listeners
    private lateinit var onNoteClickListener: OnNoteClickListener
    private lateinit var onDeleteNoteClickListener: OnDeleteNoteClickListener

    @JvmName("setOnNoteClickListener1")
    fun setOnNoteClickListener(onNoteClickListener: OnNoteClickListener) {
        this.onNoteClickListener = onNoteClickListener
    }

    @JvmName("setOnDeleteNoteClickListener1")
    fun setOnDeleteNoteClickListener(onDeleteNoteClickListener: OnDeleteNoteClickListener) {
        this.onDeleteNoteClickListener = onDeleteNoteClickListener
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Note>() {
            override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean {
                return oldItem.id == newItem.id
            }


            override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean {
                return oldItem == newItem
            }

        }
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val note: Note? = getItem(position)
        val noteViewHolder = holder as NoteViewHolder
        if (note != null) {
            noteViewHolder.bind(note)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        return NoteViewHolder(
            NoteItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ), onNoteClickListener, onDeleteNoteClickListener
        )
    }
}
