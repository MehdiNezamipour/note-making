package com.nezamipour.mehdi.notemaking.ui.foldernotes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.nezamipour.mehdi.notemaking.R
import com.nezamipour.mehdi.notemaking.adapter.OnDeleteNoteClickListener
import com.nezamipour.mehdi.notemaking.adapter.OnNoteClickListener
import com.nezamipour.mehdi.notemaking.databinding.FragmentFolderNotesBinding
import com.nezamipour.mehdi.notemaking.model.Note
import com.nezamipour.mehdi.notemaking.ui.foldernotes.paging.NotePagingAdapter
import com.nezamipour.mehdi.notemaking.utils.ui_utils.UiUtils
import com.nezamipour.mehdi.notemaking.utils.ui_utils.ViewAnimation
import com.nezamipour.mehdi.notemaking.utils.ui_utils.hideKeyboard
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject


@AndroidEntryPoint
class FolderNotesFragment : Fragment() {

    private lateinit var binding: FragmentFolderNotesBinding

    @Inject
    lateinit var adapter: NotePagingAdapter

    /* @Inject
     lateinit var onlyNoteAdapter: OnlyNoteAdapter*/
    private val viewModel: FolderNotesViewModel by viewModels()
    private var isRotate: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.folder.value = FolderNotesFragmentArgs.fromBundle(requireArguments()).folder
        viewModel.folder.value?.let { viewModel.getNotesOfFolder(it.id) }


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFolderNotesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        setObservers()
        setListeners()

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.getNotesOfFolderPaged().collectLatest { pagingData ->
                adapter.submitData(pagingData)
            }
        }

    }


    private fun setObservers() {


        // after back from dialog
        val navController = findNavController()
        val navBackStackEntry = navController.getBackStackEntry(R.id.folderNotesFragment)
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>("hide")
            ?.observe(
                viewLifecycleOwner
            ) {
                requireView().hideKeyboard()
            }
        // observer for Delete folder
        val deleteFolderObserver = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_RESUME
                && navBackStackEntry.savedStateHandle.contains("delete")
            ) {
                val result = navBackStackEntry.savedStateHandle.get<Boolean>("delete");
                if (result == true) {
                    viewModel.deleteNotesOfFolder(folderId = viewModel.folder.value!!.id)
                    viewModel.deleteFolderById(folderId = viewModel.folder.value!!.id)
                    navController.popBackStack()
                }
            }
        }
        navBackStackEntry.lifecycle.addObserver(deleteFolderObserver)
        viewLifecycleOwner.lifecycle.addObserver(LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_DESTROY) {
                navBackStackEntry.lifecycle.removeObserver(deleteFolderObserver)
            }
        })

        // observer for rename folder
        val renameFolderObserver = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_RESUME
                && navBackStackEntry.savedStateHandle.contains("rename")
            ) {
                val newTitle = navBackStackEntry.savedStateHandle.get<String>("rename");
                if (newTitle != null) {
                    if (newTitle.isNotEmpty()) {
                        viewModel.folder.value!!.title = newTitle
                        binding.folerTitleInToolbar.text = newTitle
                        viewModel.updateFolder(newTitle)
                    }
                }
            }
        }
        navBackStackEntry.lifecycle.addObserver(renameFolderObserver)
        viewLifecycleOwner.lifecycle.addObserver(LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_DESTROY) {
                navBackStackEntry.lifecycle.removeObserver(renameFolderObserver)
            }
        })

        val deleteNoteObserver = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_RESUME
                && navBackStackEntry.savedStateHandle.contains("deleteNote")
            ) {
                val result = navBackStackEntry.savedStateHandle.get<Boolean>("deleteNote");
                if (result == true) {
                    viewModel.deleteNote()
                    adapter.notifyDataSetChanged()
                    //onlyNoteAdapter.notifyDataSetChanged()
                }
            }
        }
        navBackStackEntry.lifecycle.addObserver(deleteNoteObserver)
        viewLifecycleOwner.lifecycle.addObserver(LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_DESTROY) {
                navBackStackEntry.lifecycle.removeObserver(deleteNoteObserver)
            }
        })


    }

    private fun initUi() {
        // with paging 3
        binding.notesOfFolderRecyclerView.adapter = adapter
        adapter.setOnDeleteNoteClickListener(object : OnDeleteNoteClickListener {
            override fun onClickedDelete(note: Note) {
                viewModel.note.value = note
                findNavController()
                    .navigate(FolderNotesFragmentDirections.actionFolderNotesFragmentToRemoveNoteDialogFragment())
            }

        })
        adapter.setOnNoteClickListener(object : OnNoteClickListener {
            override fun onNoteClicked(note: Note) {
                Navigation.findNavController(binding.root)
                    .navigate(
                        FolderNotesFragmentDirections.actionFolderNotesFragmentToWriteNoteFragment(
                            note
                        )
                            .setFolderId(viewModel.folder.value!!.id)
                    )
            }

        })



        binding.folerTitleInToolbar.text = viewModel.folder.value!!.title


        // normal recycler adapter memory problem
        /* binding.notesOfFolderRecyclerView.adapter = onlyNoteAdapter

         onlyNoteAdapter.setOnDeleteNoteClickListener(object : OnDeleteNoteClickListener {
             override fun onClickedDelete(note: Note) {
                 viewModel.note.value = note
                 findNavController()
                     .navigate(FolderNotesFragmentDirections.actionFolderNotesFragmentToRemoveNoteDialogFragment())
             }

         })
         onlyNoteAdapter.setOnNoteClickListener(object : OnNoteClickListener {
             override fun onNoteClicked(note: Note) {
                 Navigation.findNavController(binding.root)
                     .navigate(
                         FolderNotesFragmentDirections.actionFolderNotesFragmentToWriteNoteFragment(
                             note
                         )
                             .setFolderId(viewModel.folder.value!!.id)
                     )
             }

         })*/

    }

    private fun setListeners() {
        binding.fabAdd.setOnClickListener {
            floatingButtonAnim(it)
        }

        binding.fabAddNote.setOnClickListener {
            isRotate = false
            Navigation.findNavController(it)
                .navigate(
                    FolderNotesFragmentDirections.actionFolderNotesFragmentToWriteNoteFragment(
                        null
                    ).setFolderId(viewModel.folder.value!!.id)
                )
        }

        binding.imageViewBack.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.imageViewWriteNoteMore.setOnClickListener {
            UiUtils.showPopupMenu(
                binding.root.context,
                binding.imageViewWriteNoteMore,
                R.menu.more_option_folder_menu
            ) { menuItem ->
                when (menuItem.itemId) {
                    R.id.rename_folder -> {
                        Navigation.findNavController(binding.root)
                            .navigate(
                                FolderNotesFragmentDirections.actionFolderNotesFragmentToChangeFolderTitleDialogFragment(
                                    viewModel.folder.value!!
                                )
                            )
                    }
                    R.id.delete_folder -> {
                        Navigation.findNavController(binding.root)
                            .navigate(
                                FolderNotesFragmentDirections.actionFolderNotesFragmentToRemoveFolderDialogFragment()
                            )
                    }
                }
                false
            }
        }

        binding.dimScreen.setOnClickListener {
            floatingButtonAnim(binding.fabAdd)
        }

    }

    private fun floatingButtonAnim(it: View) {
        isRotate = ViewAnimation.rotateFab(it, !isRotate)
        if (isRotate) {
            ViewAnimation.showIn(binding.fabAddNote)
            dimScreen()
        } else {
            ViewAnimation.showOut(binding.fabAddNote)
            lightScreen()
        }
    }

    private fun dimScreen() {
        ViewAnimation.dimScreenAnim(binding.dimScreen)
    }

    private fun lightScreen() {
        ViewAnimation.lightScreenAnim(binding.dimScreen)
    }


    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FolderNotesFragment().apply {
            }
    }
}
