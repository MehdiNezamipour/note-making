package com.nezamipour.mehdi.notemaking.data.dao

import androidx.paging.PagingSource
import androidx.room.*
import com.nezamipour.mehdi.notemaking.data.entity.FolderEntity
import com.nezamipour.mehdi.notemaking.data.entity.FolderWithNotes
import com.nezamipour.mehdi.notemaking.data.entity.NoteEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NoteDao {


    // queries of notes
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNote(noteEntity: NoteEntity): Long

    @Delete
    suspend fun deleteNote(noteEntity: NoteEntity)

    @Delete
    suspend fun deleteNotes(list: List<NoteEntity>)

    @Query("DELETE FROM note WHERE  folderOfNoteId = :folderId ")
    suspend fun deleteNotesOfFolder(folderId: Long)

    @Update
    suspend fun updateNote(noteEntity: NoteEntity)

    @Query("SELECT * FROM note WHERE noteId=:noteId")
    fun getNote(noteId: Long): Flow<NoteEntity>

    // get all notes with folder and without folder
    @Query("SELECT * FROM note")
    fun getAllNotes(): Flow<List<NoteEntity>>

    // return notes that doesn't have folder
    @Query("SELECT * FROM note WHERE folderOfNoteId = 0 ")
    fun getAllNotesWithoutFolder(): Flow<List<NoteEntity>>


    // queries of folders
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFolder(folderEntity: FolderEntity): Long

    @Delete
    suspend fun deleteFolder(folderEntity: FolderEntity)


    @Query("DELETE FROM folder WHERE folderId=:folderId")
    suspend fun deleteFolderById(folderId: Long)


    @Update
    suspend fun updateFolder(folderEntity: FolderEntity)


    @Query("SELECT * FROM folder WHERE folderId=:folderId")
    fun getFolder(folderId: Long): Flow<FolderEntity>


    @Query("SELECT * FROM folder")
    fun getAllFolders(): Flow<List<FolderEntity>>


    // queries of folders with notes
    // memory problem
    @Transaction
    @Query("SELECT * FROM folder")
    fun getFoldersWithNotes(): Flow<List<FolderWithNotes>>

    // use paging 3
    @Query("SELECT * FROM note WHERE folderOfNoteId=:folderId")
    fun getNotesOfFolderPaged(folderId: Long): PagingSource<Int, NoteEntity>


    @Query("SELECT * FROM note WHERE folderOfNoteId=:folderId")
    fun getNotesOfFolder(folderId: Long): Flow<List<NoteEntity>>

}