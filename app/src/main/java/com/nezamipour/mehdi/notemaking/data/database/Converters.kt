package com.nezamipour.mehdi.notemaking.data.database

import androidx.room.TypeConverter
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object Converters {

    @TypeConverter
    fun dateToString(date: Date): String {
        //date format without leading zero
        val dateFormat: DateFormat = SimpleDateFormat("M/d/yyyy hh:mm")
        return dateFormat.format(date)
    }

    @TypeConverter
    fun stringToDate(string: String): Date {
        return SimpleDateFormat("M/d/yyyy hh:mm").parse(string)
    }

}