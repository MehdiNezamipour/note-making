package com.nezamipour.mehdi.notemaking.ui.writenote

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nezamipour.mehdi.notemaking.model.Note
import com.nezamipour.mehdi.notemaking.repository.NoteRepository
import com.nezamipour.mehdi.notemaking.utils.database_utils.DatabaseNoteMapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class WriteNoteViewModel @Inject constructor(
    private val noteRepository: NoteRepository
) :
    ViewModel() {

    var note: Note? = null
    var folderId: Long = 0L


    fun insertNoteWithFolder(folderId: Long, title: String, explain: String) {
        viewModelScope.launch {
            val note = Note(
                0,
                folderId,
                title,
                explain,
                Date()
            )
            noteRepository.insertNote(note)
        }
    }


    fun insertNoteWithoutFolder(title: String, explain: String) {
        viewModelScope.launch {
            val note = Note(
                0,
                0,
                title,
                explain,
                Date()
            )
            noteRepository.insertNote(note)
        }
    }

    fun deleteNote(note: Note) {
        viewModelScope.launch {
            noteRepository.deleteNote(note)
        }
    }

    fun updateNote(title: String, explain: String) {
        viewModelScope.launch {
            val newNote = note?.let {
                Note(
                    it.id,
                    note!!.folderOfNoteId,
                    title,
                    explain,
                    Date()
                )
            }
            newNote?.let { noteRepository.updateNote(it) }
        }
    }


}