package com.nezamipour.mehdi.notemaking.ui.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.nezamipour.mehdi.notemaking.databinding.FragmentRemoveNoteDialogBinding

class RemoveNoteDialogFragment : DialogFragment() {


    private lateinit var binding: FragmentRemoveNoteDialogBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRemoveNoteDialogBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setListeners()
    }

    private fun setListeners() {
        binding.buttonDeleteNote.setOnClickListener {
            findNavController().previousBackStackEntry?.savedStateHandle?.set(
                "deleteNote",
                true
            )
            dismiss()
        }

        binding.buttonCancel.setOnClickListener(View.OnClickListener {
            dismiss()
        })
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RemoveNoteDialogFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}